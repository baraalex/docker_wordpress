# WORDPRESS DOCKER IMAGE

### Requirements
* [docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)

This repository contains a docker compose image to build a wordpress with all its functionality and Myql, the machine with wordpress also have installed gulp and wp-cli.


## Installation
You can personalize the installation or get a new and fresh wordpress image

* Create a wp *(or a name of your choose if you want to personalize)* folder

### Personalize

1. Add your folder with ypu WP and the sql dump. ex: wordpress folder
2. At *docker-compose.yml* uncomment the lines 16 and 17
3. Change the name of the sql file at line 17. ex: wp.sql:

    >  17   - ./wordpress/wp.sql:/docker-entrypoint-initdb.d/wp.sql

4. Change the name of the sql file at line 33. ex: db.sql > wp.sql:

    >  33   - ./wordpress/:/var/www/htm

5. Change the ip of network of the docker containers
6. Change the ip of the sql file at line 15 and 36. ex: 172.25.0.102 > 172.29.0.102:

    >  15  - ipv4_address: 172.29.0.102

7. Change the ip configuration at line 43. ex: 172.25.0.102 > 172.29.0.102:

    >  -43 -  subnet: 172.29.0.0/24

8. Choose between compass or gulp at *Dockerfile* :
    1. To choose gulp just leave the file as it is
    2. To choose compass uncomment the lines 11 to 13
    3. To unchoose gulp comment the lines 16 to 19
### Install

* install with
```sh
docker-compose up --build
```

## Commands
```sh
docker-compose up
```

You can acces to the machine at http://172.25.0.101:8080

* Log into the machine
```sh
docker exec -ti test.wordpress /bin/bash
```

* Export or import DDBB
```sh
wp db export
wp db import
```

* Style work
```sh
cd /var/www/html/....
gulp
```
