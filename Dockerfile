# Pull base image.
FROM wordpress:apache
MAINTAINER Alejandro Barahona <alex.grajera@gmail.com>

#MySQL client
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y mysql-client

# compass
# RUN apt-get install -y ruby-full
# RUN gem install sass
# RUN gem install compass

# GULP
RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -
RUN apt-get install -y nodejs
RUN node --version
RUN npm install -g gulp

# wp-cli
COPY .wp-cli/wp-cli.phar /usr/local/bin/wp/wp-cli.phar
RUN chmod +x /usr/local/bin/wp/wp-cli.phar
RUN echo "alias wp='php /usr/local/bin/wp/wp-cli.phar --allow-root'" >> /root/.bashrc

# WP UPDATES
RUN chmod -R +666 /var/www/html
